# Casino Service
Casino Service is a service for gambling services.
# Build Tool
- ##### Maven
# DevOps
- ##### Bitbucket
- ##### Git
# Frameworks
#### Spring Boot
- Service build framework.
- Service Endpoint Url (`http://localhost:8080/casino`).
#### Spring Actuator
- Used for service **monitoring**.
    - All components - Url (`http://localhost:8080/manage-casino-app`).
    - Specific components - Url (`http://localhost:8080/manage-casino-app/*`).
        - ###### Health check
            - Url (`*/health`)
        - ###### Information
            - Url (`*/info`)
        - ###### Metrics
            - Url (`*/metrics`)
#### Spring Controller Advice
- Used for exception handling strategy.
#### Lombok
- Java Library used for reducing boilerplate code such as getters and setters etc.
# Run Instructions
- Clone service
    - Url (`git clone https://Lodwin@bitbucket.org/Lodwin/casino.git`)
- Run maven refresh to import all dependencies.
- Run maven [clean, install] goals to build service.
# Run With Script
- Execute script (`./run-script.sh`) that will execute casino jar
# Testing
- Use tools such as `Insomnia` or `Swagger Inspector` to consume the service.
- Find sample payloads and enpoints in the application `resources` directory.
- Main Endpoint `http://localhost:8080/casino`.
# Database Information
- H2 In-Memory Database
- url (`*/h2-console`)
- Credentials (please get credentials from `application.properties`)
# Future Improvements
- Service security layer implementation such that only authorized clients can consume the service.
    - ###### Security Mechanism
        - JWT
        - Spring Security
- Field validation, error handling, and test coverage

