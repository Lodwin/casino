package com.rank.casino;
import com.rank.casino.dto.CommonDto;
import com.rank.casino.exceptionManager.CustomExceptionMessages;
import com.rank.casino.exceptionManager.Validation;
import com.rank.casino.service.CasinoService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CasinoServiceTests {
    @Autowired
    private CasinoService casinoService;
    private CommonDto commonDto;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void balance_nullWallet() {
        exceptionRule.expect(InternalError.class);
        exceptionRule.expectMessage(CustomExceptionMessages.INTERNAL_ERROR);
        casinoService.balance(1L);
    }

    public void transactions_playerNotFound() {
        commonDto.setUsername("username");
        commonDto.setPassword("swordfish");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(CustomExceptionMessages.PLAYER_NOT_FOUND);
        casinoService.transactions(commonDto);
    }

    public void create_player_profile_missing_data() {
        commonDto.setUsername("username");
        exceptionRule.expect(Validation.class);
        exceptionRule.expectMessage(CustomExceptionMessages.VALIDATION);
        casinoService.transactions(commonDto);
    }
}
