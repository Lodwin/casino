package com.rank.casino.exceptionManager;

public class InsufficientFundException extends RuntimeException {
    public InsufficientFundException(String s) {
        super(s);
    }
}
