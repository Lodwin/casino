package com.rank.casino.exceptionManager;

public interface CustomExceptionMessages {
    String NO_CONTENT = "No transactions found for the user";
    String INSUFFICIENT_FUND = "Insufficient funds, please top up your account";
    String PLAYER_NOT_FOUND = "Player Not Found, please verify";
    String VALIDATION = "Password And Username are required";
    String INTERNAL_ERROR = "Error occurred while processing, please see error description on " +
            "console logs";
}
