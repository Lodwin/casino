package com.rank.casino.dto;

import lombok.Data;

@Data
public class BalanceDto {
    private Long playerId;
    private double balance;
}
