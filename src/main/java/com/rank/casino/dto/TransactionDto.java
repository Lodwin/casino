package com.rank.casino.dto;

import com.rank.casino.domain.TransactionType;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TransactionDto {
    private Long playerId;
    private Long transactionId;
    private double amount;
    private TransactionType transactionType;
    private LocalDateTime transactionTime;
}
