package com.rank.casino.dto;

import lombok.Data;

@Data
public class CommonDto {
    private Long playerId;
    private String username;
    private String password;
}
