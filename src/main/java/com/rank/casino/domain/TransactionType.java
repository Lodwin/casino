package com.rank.casino.domain;

public enum TransactionType {
    WAGER,
    WIN
}
