package com.rank.casino.domain;

import com.rank.casino.domain.base.CoreBaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "Transaction")
public class Transaction extends CoreBaseEntity implements Serializable {
    @Column(name = "Amount",length = 100)
    private double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "Type", length = 50)
    private TransactionType transactionType;

    @ManyToOne()
    @JoinColumn(name = "transaction_id", updatable=false, nullable = false)
    private Player player;
}
