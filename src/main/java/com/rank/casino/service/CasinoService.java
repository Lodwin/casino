package com.rank.casino.service;

import com.rank.casino.domain.Login;
import com.rank.casino.domain.Player;
import com.rank.casino.domain.Transaction;
import com.rank.casino.domain.TransactionType;
import com.rank.casino.domain.Wallet;
import com.rank.casino.dto.BalanceDto;
import com.rank.casino.dto.CommonDto;
import com.rank.casino.dto.TransactionDto;
import com.rank.casino.exceptionManager.CustomExceptionMessages;
import com.rank.casino.exceptionManager.InsufficientFundException;
import com.rank.casino.exceptionManager.NoContentExceptionHandler;
import com.rank.casino.exceptionManager.Validation;
import com.rank.casino.repository.LoginRepository;
import com.rank.casino.repository.PlayerRepository;
import com.rank.casino.repository.TransactionRepository;
import com.rank.casino.repository.WalletRepository;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@NoArgsConstructor
@Setter
@Getter
public class CasinoService implements ICasinoService {
    private static final Logger log = LoggerFactory.getLogger(CasinoService.class);
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private LoginRepository loginRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private WalletRepository walletRepository;

    private static final int LIMIT = 10;

    @Override
    public BalanceDto balance(Long playerId) {
        Player player = playerRepository.getById(playerId);
        try {
            Wallet wallet = player.getWallet();
            if (Objects.nonNull(wallet)) {
                BalanceDto balanceDto = new BalanceDto();
                balanceDto.setPlayerId(playerId);
                balanceDto.setBalance(wallet.getBalance());
                return balanceDto;
            } else {
                throw new IllegalArgumentException(CustomExceptionMessages.PLAYER_NOT_FOUND);
            }
        } catch (Exception exception){
            log.error(exception.getMessage());
            throw new InternalError(CustomExceptionMessages.INTERNAL_ERROR);
        }
    }

    @Override
    public List<TransactionDto> transactions(CommonDto commonDto) {
        List<TransactionDto> transactions = new ArrayList<>();
        Login login = loginRepository.findByUsername(commonDto.getUsername());
        if (Objects.nonNull(login) && login.getPassword().equalsIgnoreCase(commonDto.getPassword())) {
            Player player = login.getPlayer();
            List<Transaction> transactionList = player.getTransactions();
            if (transactionList.size() == 0) {
                throw new NoContentExceptionHandler();
            }
            Collections.reverse(transactionList);
            int size = transactionList.size();
            if (size > LIMIT) {
                transactionList = transactionList.subList(size - LIMIT, size);
            }
            transactionList.forEach(transaction -> {
                TransactionDto transactionDto = new TransactionDto();
                transactionDto.setPlayerId(player.getId());
                transactionDto.setTransactionId(transaction.getId());
                transactionDto.setAmount(transaction.getAmount());
                transactionDto.setTransactionType(transaction.getTransactionType());
                transactionDto.setTransactionTime(transaction.getCreateDate());
                transactions.add(transactionDto);
            });
        } else {
            throw new IllegalArgumentException(CustomExceptionMessages.PLAYER_NOT_FOUND);
        }
        return transactions;
    }

    @Override
    public void create(CommonDto commonDto) throws Validation {
        if (Objects.nonNull(commonDto)) {
            if (StringUtils.isBlank(commonDto.getPassword())
                    || StringUtils.isBlank(commonDto.getUsername())) {
                throw new Validation(CustomExceptionMessages.VALIDATION);
            }
        }
        Login login = new Login();
        login.setUsername(commonDto.getUsername());
        login.setPassword(commonDto.getPassword());
        Player player = new Player();
        player.setCreateDate(LocalDateTime.now());
        player.setLogin(login);
        player.setWallet(new Wallet());
        playerRepository.saveAndFlush(player);
    }

    @Override
    public void win(TransactionDto transactionDto) {
        if (player(transactionDto.getPlayerId()).isEmpty()) {
            throw new IllegalArgumentException(CustomExceptionMessages.PLAYER_NOT_FOUND);
        }
        Player player = player(transactionDto.getPlayerId()).get();
        Transaction transaction = new Transaction();
        transaction.setPlayer(player);
        transaction.setAmount(transactionDto.getAmount());
        transaction.setCreateDate(LocalDateTime.now());
        transaction.setTransactionType(TransactionType.WIN);
        player.getTransactions().add(transaction);
        topUp(player.getWallet(), transactionDto.getAmount());
    }

    @Override
    public void wager(TransactionDto transactionDto) {
        if (player(transactionDto.getPlayerId()).isEmpty()) {
            throw new IllegalArgumentException(CustomExceptionMessages.PLAYER_NOT_FOUND);
        }
        Player player = player(transactionDto.getPlayerId()).get();
        if (deduct(player.getWallet(), transactionDto.getAmount())) {
            Transaction transaction = new Transaction();
            transaction.setPlayer(player);
            transaction.setAmount(transactionDto.getAmount());
            transaction.setCreateDate(LocalDateTime.now());
            transaction.setTransactionType(TransactionType.WAGER);
            player.getTransactions().add(transaction);
            playerRepository.saveAndFlush(player);
        } else {
            throw new InsufficientFundException(CustomExceptionMessages.INSUFFICIENT_FUND);
        }
    }

    private Optional<Player> player(Long playerId) {
        return playerRepository.findById(playerId);
    }

    /**
     * This method adds player's winnings to the player's wallet
     */
    private void topUp(Wallet wallet, double amount) {
        double balance = wallet.getBalance() + amount;
        wallet.setBalance(balance);
        walletRepository.saveAndFlush(wallet);
    }

    /**
     * This method checks and deducts wagering amount from player wallet
     */
    private boolean deduct(Wallet wallet, double amount) {
        if (wallet.getBalance() >= amount) {
            double balance = wallet.getBalance() - amount;
            wallet.setBalance(balance);
            walletRepository.saveAndFlush(wallet);
            return true;
        }
        return false;
    }
}
