package com.rank.casino.service;

import com.rank.casino.dto.BalanceDto;
import com.rank.casino.dto.CommonDto;
import com.rank.casino.dto.TransactionDto;
import com.rank.casino.exceptionManager.Validation;

import java.util.List;

public interface ICasinoService {

    BalanceDto balance(Long playerId);

    List<TransactionDto> transactions(CommonDto commonDto);

    void create(CommonDto commonDto) throws Validation;

    void win(TransactionDto transactionDto);

    void wager(TransactionDto transactionDto);
}
