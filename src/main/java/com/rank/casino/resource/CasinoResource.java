package com.rank.casino.resource;

import com.rank.casino.dto.BalanceDto;
import com.rank.casino.dto.CommonDto;
import com.rank.casino.dto.TransactionDto;
import com.rank.casino.exceptionManager.Validation;
import com.rank.casino.service.ICasinoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Casino Application Endpoints
 */
@RestController
@RequestMapping("/casino")
public class CasinoResource {

    @Autowired
    private ICasinoService casinoService;

    @GetMapping(value = "{playerId}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public BalanceDto balance(@PathVariable Long playerId) {
        return casinoService.balance(playerId);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<TransactionDto> transactions(
            @RequestBody CommonDto commonDto
    ) {
        return casinoService.transactions(commonDto);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String create(@RequestBody CommonDto commonDto) throws Validation {
        casinoService.create(commonDto);
        return HttpStatus.CREATED.name();
    }

    @PostMapping("/wager")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String wager(@RequestBody TransactionDto transactionDto) {
        casinoService.wager(transactionDto);
        return "Wager successful";
    }

    @PostMapping("/win")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void winning(@RequestBody TransactionDto transactionDto) {
        casinoService.win(transactionDto);
    }
}
